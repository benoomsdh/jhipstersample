(function() {
    'use strict';

    angular
        .module('blogappApp')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['$scope', 'Principal', 'LoginService', '$state', 'Article'];

    function HomeController ($scope, Principal, LoginService, $state, Article) {
        var vm = this;
        vm.articles = [];
        vm.loadAll = function() {
            Article.query(function(result) {
                vm.articles = result;
            });
        };

        vm.loadAll();
        vm.account = null;
        vm.isAuthenticated = null;
        vm.login = LoginService.open;
        vm.register = register;
        $scope.$on('authenticationSuccess', function() {
            getAccount();
        });

        getAccount();

        function getAccount() {
            Principal.identity().then(function(account) {
                vm.account = account;
                vm.isAuthenticated = Principal.isAuthenticated;
            });
        }
        function register () {
            $state.go('register');
        }
    }
})();
