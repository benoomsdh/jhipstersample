(function() {
    'use strict';

    angular
        .module('blogappApp')
        .controller('ArticleDetailController', ArticleDetailController);

    ArticleDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'Article', 'User', 'Tag'];

    function ArticleDetailController($scope, $rootScope, $stateParams, entity, Article, User, Tag) {
        var vm = this;
        vm.article = entity;
        
        var unsubscribe = $rootScope.$on('blogappApp:articleUpdate', function(event, result) {
            vm.article = result;
        });
        $scope.$on('$destroy', unsubscribe);

    }
})();
