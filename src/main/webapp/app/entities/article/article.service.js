(function() {
    'use strict';
    angular
        .module('blogappApp')
        .factory('Article', Article);

    Article.$inject = ['$resource', 'DateUtils'];

    function Article ($resource, DateUtils) {
        var resourceUrl =  'api/articles/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.lastModified = DateUtils.convertDateTimeFromServer(data.lastModified);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
