/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package nl.benooms.blogapp.web.rest.dto;
