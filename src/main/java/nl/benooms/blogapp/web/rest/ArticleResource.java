package nl.benooms.blogapp.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import nl.benooms.blogapp.domain.Article;
import nl.benooms.blogapp.repository.ArticleRepository;
import nl.benooms.blogapp.security.SecurityUtils;
import nl.benooms.blogapp.web.rest.util.HeaderUtil;

/**
 * REST controller for managing Article.
 */
@RestController
@RequestMapping("/api")
public class ArticleResource {

    private final Logger log = LoggerFactory.getLogger(ArticleResource.class);
        
    @Inject
    private ArticleRepository articleRepository;
    
    /**
     * POST  /articles : Create a new article.
     *
     * @param article the article to create
     * @return the ResponseEntity with status 201 (Created) and with body the new article, or with status 400 (Bad Request) if the article has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/articles",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Article> createArticle(@Valid @RequestBody Article article) throws URISyntaxException {
        log.debug("REST request to save Article : {}", article);
        if (article.getId() != null) {
            return ResponseEntity.badRequest()
            		.headers(HeaderUtil.createFailureAlert("article", "idexists", "A new article cannot already have an ID"))
            		.body(null);
        } else if (!SecurityUtils.getCurrentUserLogin().equals(article.getUser().getLogin())){
        	return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
        			.headers(HeaderUtil.createFailureAlert("article", "creationnot allowed", "User submitting is not the author"))
        			.body(null);
        }
        Article result = articleRepository.save(article);
        return ResponseEntity.created(new URI("/api/articles/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("article", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /articles : Updates an existing article.
     *
     * @param article the article to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated article,
     * or with status 400 (Bad Request) if the article is not valid,
     * or with status 500 (Internal Server Error) if the article couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/articles",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Article> updateArticle(@Valid @RequestBody Article article) throws URISyntaxException {
        log.debug("REST request to update Article : {}", article);
        if (article.getId() == null) {
            return createArticle(article);
        }
        if (userIsAuthorOrAdmin(article.getId())){
        	Article result = articleRepository.save(article);
            return ResponseEntity.ok()
                    .headers(HeaderUtil.createEntityUpdateAlert("article", article.getId().toString()))
                    .body(result);
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
                    .headers(HeaderUtil.createFailureAlert("article", "updatenotallowed", "Update not allowed"))
                    .body(null);
        }
        

    }

    /**
     * GET  /articles : get all the articles.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of articles in body
     */
    @RequestMapping(value = "/articles",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Article> getAllArticles() {
        log.debug("REST request to get all Articles");
        List<Article> articles = articleRepository.findAllWithEagerRelationships();
        return articles;
    }

    /**
     * GET  /articles/:id : get the "id" article.
     *
     * @param id the id of the article to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the article, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/articles/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Article> getArticle(@PathVariable Long id) {
        log.debug("REST request to get Article : {}", id);
        Article article = articleRepository.findOneWithEagerRelationships(id);
        return Optional.ofNullable(article)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /articles/:id : delete the "id" article.
     *
     * @param id the id of the article to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/articles/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteArticle(@PathVariable Long id) {
        log.debug("REST request to delete Article : {}", id);
        if(userIsAuthorOrAdmin(id)){
        	articleRepository.delete(id);
        	return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("article", id.toString())).build();
        } else {
        	return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
        			.headers(HeaderUtil.createFailureAlert("article", "deletenotallowed", "Deletion not allowed"))
        			.build();
        }
        
    }
    
    private boolean userIsAuthorOrAdmin(Long id){
    	if(SecurityUtils.isCurrentUserInRole("ROLE_ADMIN")){
    		log.info("User is admin");
    		return true;
    	} else {
    		Article article = articleRepository.findOneWithEagerRelationships(id);
        	if(article != null && SecurityUtils.getCurrentUserLogin().equals(article.getUser().getLogin())){
        		log.info("User is author");
        		return true;
        	}
    	}
    	return false;
    }

}
